

<h1><center>Cycle blinker</center></h1>

*Arduino project to display on a 8x8 dot matrice a blinking arrow indicating the direction the cyclist wants to turn to.*

[TOC]

# Components

- 1 arduino
- 2 push buttons
- 2 LEDs
- 1 8x8 dot matrice
- 10 330Ω resistors
- 2 10kΩ resistors

# Wiring

## Dot Matrice
​		See https://medium.com/arduino-playground/using-a-led-matrix-with-arduino-640537b46f37

  - top1 => 330Ω resistor => D9

 - top2 => 330Ω resistor => D8

- top3 => D9

- top4 => 330Ω resistor => D2

- top5 => D13

- top6 => 330Ω resistor => D7

- top7 => 330Ω resistor => D5

- top8 => D10

  

-  bot1 => A0

 - bot2 => A2

- bot3 => 330Ω resistor => D3

- bot4 => 330Ω resistor => D4

- bot5 => A3

- bot6 => 330Ω resistor => D6

- bot7 => A1

- bot8 => D12

## LEDs

 - LED1- => 330Ω resistor => GND
- LED2- => 330Ω resistor => GND
- LED1+ => A4
- LED2+ => A5

## Buttons

 - btn1+ => A6
- btn1+ => 10kΩ resistor => GND
- btn1- => VCC
- btn2+ => A7
- btn2+ => 10kΩ resistor => GND
- btn2- => VCC

# Changelog

# License and EULA

See `LICENSE.md`

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
