/* ========================================================================== */
/*                               CYCLE BLINKER                                */
/* ========================================================================== */
/* 
	Displays on a 8x8 dot matrice a blinking arrow indicating the direction
	the cyclist wants to turn to.

	Components:
		- 1 arduino
		- 2 push buttons
		- 2 LEDs
		- 1 8x8 dot matrice
		- 10 330Ω resistors
		- 2 10kΩ resistors

	Wiring
		Dot Matrice
			See https://medium.com/arduino-playground/using-a-led-matrix-with-arduino-640537b46f37
			- top1 => 330Ω resistor => D9
			- top2 => 330Ω resistor => D8
			- top3 => D9
			- top4 => 330Ω resistor => D2
			- top5 => D13
			- top6 => 330Ω resistor => D7
			- top7 => 330Ω resistor => D5
			- top8 => D10

			- bot1 => A0
			- bot2 => A2
			- bot3 => 330Ω resistor => D3
			- bot4 => 330Ω resistor => D4
			- bot5 => A3
			- bot6 => 330Ω resistor => D6
			- bot7 => A1
			- bot8 => D12

		LEDs
			- LED1- => 330Ω resistor => GND
			- LED2- => 330Ω resistor => GND
			- LED1+ => A4
			- LED2+ => A5

		BUTTONS
			- btn1+ => A6
			- btn1+ => 10kΩ resistor => GND
			- btn1- => VCC
			- btn2+ => A7
			- btn2+ => 10kΩ resistor => GND
			- btn2- => VCC
*/

/* ========================================================================== */
/*                           DIRECTIONS DEFINITION                            */
/* ========================================================================== */
/* Holds the information for a direction */
typedef struct {
	// should the LEDs be on
	bool isOn; 
	// drawing on the dot matrice represented by 8 bytes
	// each byte being a row
	byte design[8]; 
	// pin number for the LED
	int led;
	// pin number for the button
	int button;
} Direction;


Direction right = {
	false,
	{0x08, 0x04, 0x02, 0xFF, 0xFF, 0x02, 0x04, 0x08},
	A5,
	A6
};


Direction left = {
	false,
	{0x10, 0x20, 0x40, 0xFF, 0xFF, 0x40, 0x20, 0x10},
	A4,
	A7
};


/* Holds the current direction used */
Direction *current;


/* ========================================================================== */
/*                              PINS DEFINITION                               */
/* ========================================================================== */
/* Dot matrice */
uint8_t rowPins[8] = {10, 11, 12, 13, A0, A1, A2, A3};
uint8_t colPins[8] = {2, 3, 4, 5, 6, 7, 8, 9};


/* ========================================================================== */
/*                               CONTROL TIMERS                               */
/* ========================================================================== */
/* Holds the last millis() when an action has been performed */
// last check for the buttons
unsigned long lastPush;
// last blink for the LEDs
unsigned long lastBlink;


/* ========================================================================== */
/*                                 FUNCTIONS                                  */
/* ========================================================================== */
/* Draw on a LED matrix a design <byte[8]> representing the rows */
void draw(byte design[]){
	// row iteration
	for (int row = 0; row < 8; row++) { 
		// col iteration for cleanup
		for (int k = 0; k < 8; k++){ 
			digitalWrite(colPins[k], HIGH);
		}
		// prepare to write the row
		digitalWrite(rowPins[row], HIGH); 
		// write columns
		for (int col = 0; col < 8; col++) {
			digitalWrite(colPins[7 - col], design[row] & 1 << col ? LOW : HIGH);
		}
		delay(1);
		digitalWrite(rowPins[row], LOW);
	}
}


/* ========================================================================== */
/*                                    MAIN                                    */
/* ========================================================================== */
void setup() {
	
	// Set the dot matrice pins
    for (int i = 0; i < 8; i++) {
        pinMode(colPins[i], OUTPUT);
        pinMode(rowPins[i], OUTPUT);
    }
	// Set the LEDs pins
	pinMode(right.led, OUTPUT);
	pinMode(left.led, OUTPUT);
	// Set the buttons pins
	pinMode(right.button, INPUT);
	pinMode(left.button, INPUT);

}


void loop() {

	// Button duty
	if (millis() - lastPush > 250){

		lastPush = millis();
		if (analogRead(right.button)) {
			// if the button pushed is the same as
			// the current direction,
			// the panel should be deactivated
			if (current == &right) {
				current = NULL;
				digitalWrite(right.led, LOW);
			// Otherwise, set the direction
			} else {
				current = &right;
				right.isOn = true;
				digitalWrite(left.led, LOW);
			}
		} else if (analogRead(left.button)) {
			// if the button pushed is the same as
			// the current direction,
			// the panel should be deactivated
			if (current == &left) {
				current = NULL;
				digitalWrite(left.led, LOW);
			// Otherwise, set the direction
			} else {
				current = &left;
				left.isOn = true;
				digitalWrite(right.led, LOW);
			}
		}

	}
	
	// Blink duty
	if (current && millis() - lastBlink > 250){
		lastBlink = millis();
		if ((*current).isOn) {
			digitalWrite((*current).led, HIGH);
			(*current).isOn = !(*current).isOn;
		} else {
			digitalWrite((*current).led, LOW);
			(*current).isOn = !(*current).isOn;
		}
	}
	
	// Draw the pattern on the matrix
	if (current && (*current).isOn){
		draw((*current).design);
	}

}

